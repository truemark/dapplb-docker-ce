#!/usr/bin/env bash

set -euo pipefail

[[ -z "${DAPPL_OS_TYPE+x}" ]] && >&2 echo "DAPPL_OS_TYPE is required" && exit 1
[[ "${DAPPL_OS_TYPE}" != "linux" ]] && >&2 echo "OS type is not linux" && exit 1

[[ -z "${DAPPL_OS_ARCH+x}" ]] && >&2 echo "DAPPL_OS_ARCH is required" && exit 1
[[ "${DAPPL_OS_ARCH}" != "amd64" ]] && >&2 echo "architecture is not amd64" && exit 1

[[ -z "${DAPPL_OS_DISTRO+x}" ]] && >&2 echo "DAPPL_OS_DISTRO is required" && exit 1
[[ "${DAPPL_OS_DISTRO}" != "ubuntu" ]] && >&2 echo "Oracle Linux is required" && exit 1

[[ $EUID -ne 0 ]] && >&2 echo "must be run as root user" && exit 1

cd "$(dirname "${0}")" || exit 1

echo "-------------------------------------------------------------------------------"
echo "Prerequisites"
echo "-------------------------------------------------------------------------------"

apt-get -qq update
apt-get -qq install apt-transport-https ca-certificates curl gnupg lsb-release nvme-cli xfsprogs

cat > /etc/sysctl.d/95-docker.conf <<-EOF
fs.inotify.max_user_watches=524288
fs.inotify.max_user_instances=16384
fs.file-max=5242880
vm.max_map_count=262144
EOF
sysctl --system

echo "-------------------------------------------------------------------------------"
echo "Disk Setup"
echo "-------------------------------------------------------------------------------"
mkfs.xfs -f -n ftype=1 "$(./tmdisk 1)"
echo "UUID=$(blkid -s UUID -o value "$(./tmdisk 1)") /var/lib/docker xfs defaults 0 2" \
| tee -a /etc/fstab
mkdir -p /var/lib/docker
mount /var/lib/docker

echo "-------------------------------------------------------------------------------"
echo "Docker CE Install"
echo "-------------------------------------------------------------------------------"

curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
| sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
> /etc/apt/sources.list.d/docker.list

apt-get -qq update
apt-get -qq install docker-ce docker-ce-cli containerd.io docker-compose
